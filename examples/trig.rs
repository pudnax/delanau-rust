extern crate rand;
use delanau_rust;

use std::iter::repeat_with;

fn main() {
    for i in 2..8 {
        let n = 10usize.pow(i);
        let points: Vec<_> = repeat_with(rand::random)
            .map(|(x, y)| delanau_rust::Point { x, y })
            .take(n)
            .collect();

        let now = std::time::Instant::now();
        let result =
            delanau_rust::triangulate(&points).expect("No triangulation exists for this input.");
        let elapsed = now.elapsed();

        println!(
            "Triangulated {} points in {}.{}s.\nGenerated {} triangles. Convex hull size: {}\n",
            n,
            elapsed.as_secs(),
            elapsed.subsec_millis(),
            result.len(),
            result.hull.len()
        );
    }
}
